// const array = [1, 2, 3, 5, 20, 42]
// const arrayStrings = ['a', 'b', null, 12]

// console.log(array[3])
// console.log(array[array.length -1])

// array[0] = 'привет'
// // console.log(array)
// array[array.length] = 'becon'
// console.log(array)

const inputElement = document.getElementById('title')
const createBtn = document.getElementById('create')
const listElement = document.getElementById('list')

// console.log(inputElement.value)

const notes = ['zapisat block pro massivy', 'sozdat reklamu']



function render() {
    listElement.insertAdjacentHTML('beforeend', getNoteTemplate(notes[0]))
    listElement.insertAdjacentHTML('beforeend', getNoteTemplate(notes[1]))
}
     render()


createBtn.onclick = function () {
    if (inputElement.value.length === 0) {
        return
    } 

    // listElement.innerHTML = 
    listElement.insertAdjacentHTML('beforeend',
        getNoteTemplate(inputElement.value)
    
    )
    inputElement.value = ''
}

function getNoteTemplate(title) {
    return
        `<li
            class="list-group-item d-flex justify-content-between 
            align-items-center"
        >
            <span>${title}</span>
            <span>
            <span class="btn btn-small btn-success">&check;</span>
            <span class="btn btn-small btn-danger">&times;</span>
            </span>
        </li>`
}