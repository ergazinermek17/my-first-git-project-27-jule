// Number

// const num = 2   //integer
// const float = 43.3  //float
// const pow = 10e3

// console.log(Number.MAX_SAFE_INTEGER)
// console.log(Number.MIN_SAFE_INTEGER)
// console.log(Number.MAX_VALUE)
// console.log(Number.MIN_VALUE)
// console.log(Number.POSITIVE_INFINITY)
// console.log(Number.NEGATIVE_INFINITY)
// console.log(2/0)
// console.log(typeof NaN)
// const weird = 2/ undefined
// console.log(Number.isNaN(weird))
// console.log(Number.isNaN(42))
// console.log(Number.isFinite(Infinity))

// const stringInt = '42'
// const stringFloat = '42.42'
// console.log(Number.parseInt(stringInt) + 2)
// console.log(Number(stringInt) + 2)
// console.log(+stringInt + 2)

// console.log(parseInt(stringFloat) + 2)
// console.log(+stringFloat + 2)

// console.log(0.4+0.2)
// console.log(+(0.4+0.2).toFixed(1))
// console.log(parseFloat((0.4+0.2).toFixed(1)))

// BigInt

// console.log(typeof 90071992547409919999999n)

// // console.log(10n - 4)// error
// console.log(parseInt(10n) - 4)
// console.log(10n - BigInt(4))

//3 Math
console.log(Math.E)
console.log(Math.PI)

console.log(Math.sqrt(25))
console.log(Math.pow(5, 2))
console.log(Math.abs(-42))
console.log(Math.max(42, 12, 23, 11, 422))
console.log(Math.min(42, 12, 23, 11, 422))
console.log(Math.floor(4.9))//округляет в меньшую сторону
console.log(Math.ceil(4.9))
console.log(Math.round(4.9))
console.log(Math.trunc(4.9))
console.log(Math.random())

//4 example
function getRandomBetween(min, max) {
    return Math.floor( Math.random() * (max - min + 1) + min ) 
}

console.log(getRandomBetween(10, 42))



